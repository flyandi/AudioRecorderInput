console.log("Building AudioRecorderInput");

var exec = require('child_process').exec;

/** (Minimize Javascript) */
['audiorecinput', 'audiorecinput.worker'].forEach(function(n, i) {
	exec('uglifyjs src/'+n+'.js > dist/'+n+'.min.js');
});